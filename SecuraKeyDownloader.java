package com.pax.settings.secura;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.pax.device.Device;
import com.pax.edc.R;
import com.pax.eemv.utils.Tools;
import com.pax.gl.utils.impl.Algo;
import com.pax.pay.app.FinancialApplication;
import com.pax.poslib.gl.convert.ConvertHelper;
import com.pax.settings.SysParam;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

public class SecuraKeyDownloader extends AppCompatActivity {

    private Button btnConnectAndReceive;
    private TextView txtLogView;
    private EditText txtUrl;
    private OkHttpClient client;

    private static String url = "ws://192.168.8.100:9000";

    class ProtocolMessage {
        public static final String SEND_TID_SNO = "send-sno-tid";
        public static final String INSTALL_KEY = "install-key";
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secura_key_downloader);

        btnConnectAndReceive = findViewById(R.id.btnConnectAndReceive);
        txtLogView = findViewById(R.id.txtLogView);
        txtUrl = findViewById(R.id.txtUrl);
        url = txtUrl.getText().toString();

        btnConnectAndReceive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (connected) {
                    setStatus("Already connected to the server");
                    Device.beepErr();
                    return;
                }

                FinancialApplication.getApp().runInBackground(new Runnable() {
                    @Override
                    public void run() {
                        connectAndReceive();
                    }
                });
            }
        });
    }

    private boolean connected = false;

    private void connectAndReceive() {
        if (client == null)
            client = new OkHttpClient();


        url = txtUrl.getText().toString();
        Request request = new Request.Builder().url(url).build();
        setStatus("Connecting to server \n" + url + "\n\n");

        WebSocketListener webSocketListener = new WebSocketListener() {
            @Override
            public void onOpen(WebSocket webSocket, Response response) {
                connected = true;
                setStatus("Connected..");
                webSocket.send("hello");
            }

            @Override
            public void onMessage(WebSocket webSocket, String serverData) {
                if (serverData.equalsIgnoreCase("ping"))
                    webSocket.send("pong");

                try {
                    JSONObject jsonMessage = new JSONObject(serverData);
                    boolean error = jsonMessage.getBoolean("error");
                    String command = jsonMessage.getString("command");
                    String data = jsonMessage.getString("data");


                    //setStatus("From server \n" + jsonMessage.toString());
                    if (error) {
                        setStatus("Error  : " + data);
                        return;
                    }

                    Device.beepPrompt();

                    if (command.equalsIgnoreCase(ProtocolMessage.SEND_TID_SNO)) {
                        //the serial number and the terminal id must be sent to the server
                        String sno = Build.SERIAL;
                        String tid = FinancialApplication.getAcqManager().findAllAcquirers().get(0).getTerminalId();

                        JSONObject tmp = new JSONObject();
                        tmp.put("serialNo", sno);
                        tmp.put("terminalId", tid);

                        String toSend = createJsonString(false, ProtocolMessage.SEND_TID_SNO, tmp);
                        webSocket.send(toSend);
                        setStatus("Sending the serial no " + sno);
                    } else if (command.equalsIgnoreCase(ProtocolMessage.INSTALL_KEY)) {
                        //data field contains the clear pin key json object
                        JSONObject keyData = new JSONObject(data);
                        String primaryKey = null;
                        String secondaryKey = null;

                        if (keyData.has("primary")) {
                            //a key is destined to be installed for the primary bank
                            primaryKey = keyData.getString("primary");
                            setStatus("A primary key received " + primaryKey);
                        }

                        if (keyData.has("secondary")) {
                            //a key is destined to be installed for the secondary bank
                            secondaryKey = keyData.getString("secondary");
                            setStatus("A secondary key received " + secondaryKey);
                        }

                        //artificial delay
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        try {
                            String MASTER_KEY = "54DCBF79AEB970329E97B98651E619C1";

                            //encrypt the clear key by the master key
                            byte iv[] = new byte[8];

                            byte[] encryptedKey = Algo.des(Algo.ECryptOperation.ENCRYPT,
                                    Algo.ECryptOption.ECB,
                                    Algo.ECryptPaddingOption.NO_PADDING,
                                    Tools.str2Bcd(primaryKey),
                                    Tools.str2Bcd(MASTER_KEY), iv);


                            //injecting the encrypted clear key by master key
                            setStatus("Injecting encrypted key\n" + Tools.bcd2Str(encryptedKey));
                            setStatus("Clear key is\n" + primaryKey);

                            int masterKeyIndex = SysParam.getInstance().getInt(R.string.MK_INDEX);

                            Device.writeTMK(masterKeyIndex,Tools.str2Bcd(MASTER_KEY));
                            Device.writeTPK(encryptedKey, null);

                            byte[]  clearKey = Algo.des(Algo.ECryptOperation.DECRYPT,
                                    Algo.ECryptOption.CBC,
                                    Algo.ECryptPaddingOption.NO_PADDING,encryptedKey,Tools.str2Bcd(MASTER_KEY),iv);

                            String strClearKey = Tools.bcd2Str(clearKey);


                            setStatus("Keys injected");
                            if (secondaryKey != null) {
                                //install the secondary key
                            }


                            String toServer = createJsonString(false, ProtocolMessage.INSTALL_KEY, null);
                            webSocket.send((toServer));
                            setStatus("Sending the key acknowledgement");
                            Device.beepOk();
                        } catch (Exception ex) {
                            String toServer = createJsonString(true, ProtocolMessage.INSTALL_KEY, "Failed to inject the key");
                            webSocket.send((toServer));
                            setStatus("Sending the key acknowledgement " + ex.getMessage());
                            Device.beepErr();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onClosed(WebSocket webSocket, int code, String reason) {
                connected = false;
                setStatus("Connection closed");
            }

            @Override
            public void onFailure(WebSocket webSocket, Throwable t, @Nullable Response response) {
                connected = false;
                setStatus("Failure occurred");
            }
        };

        client.newWebSocket(request, webSocketListener);
        client.dispatcher().executorService().shutdown();
    }


    private void setStatus(final String status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                txtLogView.append(status + '\n');
            }
        });
    }

    private String createJsonString(boolean error, String command, Object data) throws JSONException {
        JSONObject tmp = new JSONObject();
        tmp.put("error", error);
        tmp.put("command", command);
        tmp.put("data", data);

        return tmp.toString();
    }
}